import 'dart:io';

class MainMenu {
  var name = [];
  var price = [];
  var sugar = [];
  var detail = [];

P1Menu() {
  print("ยินดีต้อนรับสู่ตู้เต่าบิน");
  print("1.เมนูแนะนำ");
  print("2.กาแฟ");
  print("3.ชา");
  print("4.นม โกโก้ และคาราเมล");
  print("5.โปรตีนเชค");
  print("6.น้ำโซดา และอื่น ๆ");
  print("โปรดระบุเลขเพื่อเลือก: ");
  print("-----------------------------");
  int MenuDrink = int.parse(stdin.readLineSync()!);
  p1Menu(MenuDrink);
}

p1Menu (int MenuDrink){
  print("-----------------------------");
  switch (MenuDrink){
    case 1:
      return Recommended();
    case 2:
      return Coffee();
    case 3:
      return Tea();
    case 4:
      return Milk();
    case 5:
      return Protein();
    case 6:
      return Soda();
  }
}

Coffee(){
  print("0.ย้อนกลับ");
  print("1.กาแฟดำร้อน 30 บาท");
  print("2.มอคค่าร้อน 40 บาท");
  print("3.กาแฟดำเย็น 35 บาท");
  print("4.คาปูชิโน่เย็น 45 บาท");
  print("5.ลาเต้เย็น 40 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Coffee = int.parse(stdin.readLineSync()!);
  p2Coffee(Coffee);
}

p2Coffee (int Coffee){
  print("-----------------------------");
  switch (Coffee){
    case 0:
      return P1Menu();
    case 1:
      price.add(30);
      name.add("กาแฟดำร้อน");
      return Sugar(); 
    case 2:
      price.add(40);
      name.add("มอคค่าร้อน");
      return Sugar(); 
    case 3:;
      price.add(35);
      name.add("กาแฟดำเย็น");
      return Sugar(); 
    case 4:;
      price.add(45);
      name.add("คาปูชิโน่เย็น");
      return Sugar(); 
    case 5:
      price.add(40);
      name.add("ลาเต้เย็น");
      return Sugar(); 
  }
}

Milk(){
  print("0.ย้อนกลับ");
  print("1.นมคาราเมลร้อน 35 บาท");
  print("2.โกโก้ร้อน 35 บาท");
  print("3.นมร้อน 30 บาท");
  print("4.โกโก้เย็น 35 บาท");
  print("5.นมชมพูเย็น 40 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Milk = int.parse(stdin.readLineSync()!);
  p2Milk(Milk);
}

p2Milk (int Milk){
  print("-----------------------------");
  switch (Milk){
    case 0:
      return P1Menu();
    case 1:
      price.add(35);
      name.add("นมคาราเมลร้อน");
      return Sugar(); 
    case 2:
      price.add(35);
      name.add("โกโก้ร้อน");
      return Sugar();  
    case 3:
      price.add(30);
      name.add("นมร้อน");
      return Sugar();  
    case 4:
      price.add(35);
      name.add("โกโก้เย็น");
      return Sugar();  
    case 5:
      price.add(40);
      name.add("นมชมพูเย็น");
      return Sugar();  
  }
}

Protein(){
  print("0.ย้อนกลับ");
  print("1.มัชฉะโปรตีน 60 บาท");
  print("2.โกโก้โปรตีน 60 บาท");
  print("3.ชาไต้หวันโปรตีน 60 บาท");
  print("4.นมโปรตีน 55 บาท");
  print("5.โปรตีนเปล่า 55 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Protein = int.parse(stdin.readLineSync()!);
  p2Protein(Protein);
}

p2Protein (int Protein){
  print("-----------------------------");
  switch (Protein){
    case 0:
      return P1Menu();
    case 1:
      price.add(60);
      name.add("มัทฉะโปรตีน");
      return Sugar();  
    case 2:
      price.add(60);
      name.add("โกโก้โปรตีน");
      return Sugar();  
    case 3:
      price.add(60);
      name.add("ชาไต้หวันโปรตีน");
      return Sugar(); 
    case 4:
      price.add(55);
      name.add("นมโปรตีน");
      return Sugar(); 
    case 5:
      price.add(55);
      name.add("โปรตีนเปล่า");
      return Sugar(); 
  }
}

Recommended(){
  print("0.ย้อนกลับ");
  print("1.นมสตอเบอรี่ปั่น 45 บาท");
  print("2.โกโก้สตรอเบอรี่ปั่น 45 บาท");
  print("3.โอริโอ้ปั่นภูเขาไฟ 55 บาท");
  print("4.โกโก้เย็นปั่น 40 บาท");
  print("5.มัทฉะลาเต้ปั่น 50 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Recommended = int.parse(stdin.readLineSync()!);
  p2Recommended(Recommended);
}

p2Recommended (int Recommended){
  print("-----------------------------");
  switch (Recommended){
    case 0:
      return P1Menu();
    case 1:
      price.add(45);
      name.add("นมสตอเบอรี่ปั่น");
      return Sugar();  
    case 2:
      price.add(45);
      name.add("โกโก้สตรอเบอรี่ปั่น");
      return Sugar();  
    case 3:
      price.add(55);
      name.add("โอริโอ้ปั่นภูเขาไฟ");
      return Sugar(); 
    case 4:
      price.add(40);
      name.add("โกโก้เย็นปั่น");
      return Sugar();  
    case 5:
      price.add(50);
      name.add("มัทฉะลาเต้ปั่น");
      return Sugar(); 
  }
}

Tea(){
  print("0.ย้อนกลับ");
  print("1.ชาไทยร้อน 35 บาท");
  print("2.ชาขิงร้อน 20 บาท");
  print("3.น้ำกัญชาร้อน 30 บาท");
  print("4.เก๊กฮวยเย็น 20 บาท");
  print("5.ชาไทยเย็น 40 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Tea = int.parse(stdin.readLineSync()!);
  p2Tea(Tea);
}

p2Tea (int Tea){
  print("-----------------------------");
  switch (Tea){
    case 0:
      return P1Menu();
    case 1:
      price.add(35);
      name.add("ชาไทยร้อน");
      return Sugar(); 
    case 2:
      price.add(20);
      name.add("ชาขิงร้อน");
      return Sugar();  
    case 3:
      price.add(30);
      name.add("น้ำกัญชาร้อน");
      return Sugar(); 
    case 4:
      price.add(20);
      name.add("เก๊กฮวยเย็น");
      return Sugar();  
    case 5:
      price.add(40);
      name.add("ชาไทยเย็น");
      return Sugar(); 
  }
}

Soda(){
  print("0.ย้อนกลับ");
  print("1.เป็ปซี่น้ำแข็ง 15 บาท");
  print("2.ชูกำลังโซดา 15 บาท");
  print("3.น้ำมะนาวโซดา 20 บาท");
  print("4.น้ำกัญชาโซดา 40 บาท");
  print("5.น้ำแดงโซดา 20 บาท");
  print("โปรดระบุเลขเพื่อเลือก");
  print("-----------------------------");
  int Soda = int.parse(stdin.readLineSync()!);
  p2Soda(Soda);
}

p2Soda (int Soda){
  print("-----------------------------");
  switch (Soda){
    case 0:
      return P1Menu();
    case 1:
      price.add(15);
      name.add("เป็ปซี่น้ำแข็ง");
      return Sugar(); 
    case 2:
      price.add(15);
      name.add("ชูกำลังโซดา");
      return Sugar(); 
    case 3:
      price.add(20);
      name.add("น้ำมะนาวโซดา");
      return Sugar(); 
    case 4:
      price.add(40);
      name.add("น้ำกัญชาโซดา");
      return Sugar();  
    case 5:
      price.add(20);
      name.add("น้ำแดงโซดา");
      return Sugar();
  }
}

Sugar(){
  print("โปรดเลือก ระดับความหวาน");
  print("1.ไม่ใส่น้ำตาล");  
  print("2.หวานน้อย");
  print("3.หวานพอดี");
  print("4.หวานมาก");
  print("5.หวาน 3 โลก");
  print("-----------------------------");
  int Sugar = int.parse(stdin.readLineSync()!);
  p3Sugar(Sugar);
}

p3Sugar (int Sugar){
  print("-----------------------------");
  switch (Sugar){
    case 1:
      sugar.add("ไม่ใส่น้ำตาล");
      return Detail();
    case 2:
      sugar.add("หวานน้อย");
      return Detail();
    case 3:
      sugar.add("หวานพอดี");
      return Detail();
    case 4:
      sugar.add("หวานมาก");
      return Detail();
    case 5:
      sugar.add("หวาน 3 โลก");
      return Detail();
  }
}

Detail(){
  print("โปรดเลือก รับฝา & รับหลอด");
  print("1.รับฝา");  
  print("2.รับหลอด");
  print("3.รับทั้งหลอดและฝา");  
  print("-----------------------------");
  int Detail = int.parse(stdin.readLineSync()!);
  p4Detail(Detail);
}

p4Detail(int Detail){
  print("-----------------------------");
  switch (Detail){
    case 1:
      detail.add("รับฝา");
      return printOrder();
    case 2:
      detail.add("รับหลอด");
      return printOrder();
    case 3:
      detail.add("รับทั้งหลอดและฝา");
      return printOrder();
  }
}

void printOrder(){
  print("รายการคำสั่งซื้อ");
  for(int i = 0 ; i < name.length;i++){
    print(name[i].toString() + " " +price[i].toString() + " บาท " + sugar[i].toString() + " " + detail[i].toString());
  }
  print("-----------------------------");
  print("คุณลูกค้าสะดวกชำระเงินแบบไหนครับ");
  print("1.เงินสด");
  print("2.พร้อมเพย์");
  print("-----------------------------");
  int Pay = int.parse(stdin.readLineSync()!);
  print("-----------------------------");
  p5Pay(Pay);
  print("ขอบคุณที่ใช้บริการครับ");
}

p5Pay(int Pay){
  if(Pay == 1){
    print("เชิญใส่เงินในช่องทางด้านขวามือได้เลยครับ");
  }
  else if(Pay == 2){
    print("เชิญสแกน QR Code ได้เลยครับ");
  }
}
}